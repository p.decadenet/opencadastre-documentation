.. _importmajic3:

#######################################################
Importation des fichiers MAJIC 3
#######################################################

Ce traitement permet d'importer et de formater les données majic 3

Ce traitement est le premier tratiement du processus d'intégration annuel des données.

Les fichiers fournis sont au nombre de 6 :

- NBAT : propriétés non baties ('ensemble des parcelles et des subdivisions fiscales cadastrées)
- PROP : propriétaires
- BATI : propriétés baties (l'ensemble des informations concernant le local et la partie d’évaluation dite PEV)
- PDLL: PDL - Lots (description des propriétés divisées en lots)
- LLOC : lot-local (table de correspondance entre les identifiants des locaux et les indicatifs des lots correspondants)
- FANR_13 : voies et lieux-dits

Des documents descriptifs sont également fournis annuellement.
C'est sur la base de ces derniers qu'évolue openCadastre.

- Copier les fichiers dans le répertoire serveur que vous avez spécifié dans la variable $import_majic3_chemin du fichier dyn/var.inc.php.

Modifier les noms de fichiers dans ce même fichier de configuration.

Il est ensuite indispensable d'épurer les 6 fichiers de données des caractères NULL en les remplacant par des espaces,
par exemple en utilisant notepad++ :

.. image:: ../_static/traitement/trait_magic_sup_nul.jpg

Ouvrir la page du traitement via le menu Traitements / Import. Majic3

.. image:: ../_static/traitement/trait_magic_menu.jpg

Modifier si nécessaire les champs:

- version des fichiers: il s'agit du millésime du format des données majic 3
- concerne l'exercice: il s'agit du millésime des données majic 3
- chemin des fichiers majic3: dossier serveur contenant les fichiers majic3 (préalablement épurés des caractères null), vu du serveur
- numéro de lot: 3 caractères qui seront repris dans la base de données afin de pouvoir gérer plusieurs lots de données

Pour compléter les deux premiers champs, prenons l'exemple suivant: fin 2013 nous est transmis les données arrêtées au 31/12/2012 au format 2013 soit, dans le premier champ 2013 et dans le second 2012.

Le même numéro de lot devra être repris dans le traitement d'importation edigeo relatif au plan cadastral numérisé afin d'assurer l'intégrité des données.

Cette procédure s’appuie sur les canevas de script sql présent dans les répertoires data/pgsql/COMMUN et data/pgssql/[VERSION], version correspondant à votre saisie dans le champ « Version des fichiers »

Il y a 6 étapes obligatoires que vous pouvez exécuter l’une après l’autre ou toutes automatiquement (Enchainer les traitements). Cette dernière possibilité n’est toutefois pas préconisée.

- Choisir la première étape et cliquer sur "lancer le traitement" 

1 - Suppression des contraintes relationnelles : On ôte les contraintes référentielles afin d’optimiser la procédure. 
  (environ 1mn)

.. image:: ../_static/traitement/trait_magic_etape1.jpg

- Cliquer sur "2"

2 - Importation des données brutes : importe les données brutes dans des tables temporaires 1 table par fichier et 1 enregistrement par ligne
  (environ 1mn)

.. image:: ../_static/traitement/trait_magic_etape2.jpg

- Cliquer sur "3"

3 - Purges des données préalablement intégrées : supprime les données préalablement importées (critères "Concerne l'exercice" et "numéro de lot")
  (< 1mn si vide)

.. image:: ../_static/traitement/trait_magic_etape3.jpg

- Cliquer sur "4"

4 - Intégration et formatage des données : insérer et formate les données brutes importées à l'étape 2 en respectant le modèle de données openCadastre
  (environ 10mn)

.. image:: ../_static/traitement/trait_magic_etape4.jpg

- Cliquer sur "5"

5 - Rétablissement des contraintes relationnelles : repose les contraintes réferentielles ôtées à l'étape 1 afin d'assurer l'intégrité des données
  (environ 1mn)

.. image:: ../_static/traitement/trait_magic_etape5.jpg

- Cliquer sur "6"

6 - Purge des données temporaires : supprime les données brutes importées à l'étape 2
  (environ 1mn)

.. image:: ../_static/traitement/trait_magic_etape6.jpg

- Nous pouvons maintenant passer au traitement suivant d'intégration du plan cadastral numérisé au format Edigéo.


