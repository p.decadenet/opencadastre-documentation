.. _anonymisation:

#########################################################
Anonymisation de tout ou partie des données propriétaires
#########################################################

OpenCadastre vous permet d'anonymiser tout ou partie des données propriétaires en fonction 

- du type de personne physique ou morale
- de la nature de la personne

Ce traitement permet de vider les colonnes suivantes

- ccoqua : Code qualité de personne physique - 1, 2 ou 3
- dsglpm :  sigle de personne morale
- ddenom : Dénomination de personne physique ou morale
- gtyp3 : type de la 3eme ligne d’adresse
- dlign3 : 3eme ligne d’adresse
- gtyp4 : type de la 4eme ligne d’adresse
- dlign4 : 4eme ligne d’adresse
- gtyp5 : type de la 5eme ligne d’adresse
- dlign5 : 5eme ligne d’adresse
- gtyp6 : type de la 6eme ligne d’adresse
- dlign6 : 6eme ligne d’adresse
- dqualp : Qualité abrégée - M, MME ou MLE
- dnomlp : Nom d’usage
- dprnlp : Prénoms associés au nom d’usage
- jdatnss : date de naissance - sous la forme jj/mm/aaaa          
- dldnss : lieu de naissance
- epxnee : mention du complément - EPX ou NEE si complément
- dnomcp : Nom complément
- dprncp : Prénoms associés au complément

de la table des propriétaires

Vous pouvez lancer ce traitement en fonction deux critères :

- par type de personne physique ou morale

dans le menu général cliquer sur "Nomenclatures" / "Pers. Physique/morale" 

.. image:: ../_static/traitement/trait_anomym_pers_menu.jpg

ouvrir le formulaire correspondant au type de personne que vous voulez rendre anonyme, dans notre exemple les personnes physiques

.. image:: ../_static/traitement/trait_anomym_pers_form.jpg

cliquer ensuite sur "rendre anonyme" 

.. image:: ../_static/traitement/trait_anomym_pers_trait.jpg

le formulaire de traitement est pré-rempli en fonction de votre choix, cliquer maintenant sur "lancer le traitement"

.. image:: ../_static/traitement/trait_anomym_pers_exec.jpg

les propriétaires correspondants sont maintenant anonymisés.

- par nature de personne

dans le menu général cliquer sur "Nomenclatures" / "Nature personne" 

.. image:: ../_static/traitement/trait_anomym_nature_menu.jpg

ouvrir le formulaire correspondant à la nature de personne que vous voulez rendre anonyme, dans notre exemple les caisses d'assurance agricole

.. image:: ../_static/traitement/trait_anomym_nature_form.jpg

cliquer ensuite sur "rendre anonyme" 

.. image:: ../_static/traitement/trait_anomym_nature_trait.jpg

le formulaire de traitement est pré-rempli en fonction de votre choix, cliquer maintenant sur "lancer le traitement"

.. image:: ../_static/traitement/trait_anomym_nature_exec.jpg

les propriétaires correspondants sont maintenant anonymisés.

