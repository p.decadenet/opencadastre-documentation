.. _generationuf:

###############################
Génération des Unités Foncières
###############################


Ce traitement permet de générer les unités foncières, parcelles adjacentes appartenant au même compte communal, pour un exercice donné.


Ouvrir la page du traitement via le menu Traitements / Génération  UF 

.. image:: ../_static/traitement/trait_uf_menu.jpg

Choisir l'exercice correspondant au millésime des données importées et cliquer sur "lancer le traitement" (10 min).

.. image:: ../_static/traitement/trait_uf_exec.jpg
