.. _rub_proprietaire:

#############
Propriétaires
#############

Important: les notes sont extraites de la documentation fournie avec les données.

.. note ::

  Le fichier des propriétaires regroupe par direction des informations concernant le compte communal et la
  personne.
  
  Le compte communal est composé de l'ensemble des personnes exerçant des droits concurrents sur un
  ou plusieurs biens d'une commune. L'identification du compte communal est inchangée par rapport au
  système précédent, à savoir un numéro d'ordre établi par commune, dans des séries alphabétiques pour
  les personnes physiques, dans les séries « + » ou « * » pour les personnes morales.

  Le fichier permet de disposer, pour un compte communal donné, des personnes (dans la limite de six)
  titulaires du compte, avec la désignation « complète » ou structurée, le droit exercé, l'adresse formatée
  et éventuellement codifiée.
  
------------------
Modèle relationnel
------------------
.. image:: ../_static/modeles/proprietaire.jpg

----------
Les tables
----------

.. _comptecommunal:

********************************
Compte communal (comptecommunal)
********************************

Cette table est déduite de la liste des propriétaires.

.. image:: ../_static/utilisation/comptecommunal.jpg

Champ clé primaire :

- commune character varying(10) NOT NULL : code identifiant le compte communal ([annee][ccodep][dnupro] caractère + remplacé par ¤ et espace remplacé par 0)

Autres colonnes :

- annee character varying(4): millésime des données
- ccodep character varying(2): Code département - Code département INSEE
- ccodir character varying(1): Code direction - Code direction dge
- ccocom character varying(3): Code commune - code commune définie par Majic2
- dnupro character varying(6): code du Compte communal
- ajoutcoherence character varying(1): indicateur de cohérence - N: présent dans le fichier majic3 des propriétaires, O: absent du fichier majic3 des propriétaires, ajouté pour assurer l'intégrité reférentielle avec les tables dépendantes
- lot character(3): lot d'importation des données

Champs clés étrangères :

- néant

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- local10: :ref:\'local10`
- lots: :ref:`lots`
- parcelle: :ref:`parcelle`
- pdl: :ref:`pdl`
- proprietaire: :ref:`proprietaire`
- suf: :ref:\`suf`

.. _proprietaire:


***************************
Propriétaire (proprietaire)
***************************

cf. définition en entête de rubrique

.. image:: ../_static/utilisation/proprietaire.jpg

Champ clé primaire :

- proprietaire character varying(20) NOT NULL : code identifiant le propriétaire ([annee][ccodep][dnupro][dnulp][dnuper] caractère + remplacé par ¤ et espace remplacé par 0)

Autres colonnes :

* Entête

  - annee character varying(4): millésime des données
  - ccodep character varying(2): Code département - Code département INSEE
  - ccodir character varying(1): Code direction - Code direction dge
  - ccocom character varying(3): Code commune - code commune définie par Majic2
  - dnupro character varying(6): code du compte communal
  - dnulp character varying(2): numéro de libellé partiel - 01 à 06
  - ccocif character varying(4): code cdif
  - dnuper character varying(6): numéro de personne dans le cdif 
  - gdesip character varying(1): indicateur du destinataire de l’avis d’imposition - 1 = oui, 0 = non
  - *gnexcf character varying(2): code exo ecf (indisponible)*
  - *dtaucf character varying(3): taux exo ecf (indisponible)*
  - dsglpm character varying(10): sigle de personne morale
  - dformjur character varying(4): Forme juridique abrégée
  
* Données générales 

  - ddenom character varying(60): Dénomination de personne physique ou morale
  - dlign3 character varying(30): 3eme ligne d’adresse
  - dlign4 character varying(36): 4eme ligne d’adresse
  - dlign5 character varying(30): 5eme ligne d’adresse
  - dlign6 character varying(32): 6eme ligne d’adresse X Codification de l’adresse
  
* Codification de l’adresse

  - ccopay character varying(3): code de pays étranger et TOM - non servi pour France métropole et Dom
  - ccodep1a2 character varying(2): Code département de l’adresse
  - ccodira character varying(1): Code direction de l’adresse
  - ccocom_adr character varying(3): Code commune de l’adresse
  - ccovoi character varying(5): Code majic2 de la voie
  - ccoriv character varying(4): Code rivoli de la voie
  - dnvoiri character varying(4): numéro de voirie
  - dindic character varying(1): indice de répétition de voirie
  - ccopos character varying(5): Code postal X Dénomination formatée de personne physique
  
* Dénomination formatée de personne physique

  - *dnirpp character varying(10): zone à blanc (indisponible)*
  - dqualp character varying(3): Qualité abrégée - M, MME ou MLE
  - dnomlp character varying(30): Nom d’usage
  - dprnlp character varying(15): Prénoms associés au nom d’usage
  - jdatnss date: date de naissance - sous la forme jj/mm/aaaa
  - dldnss character varying(58): lieu de naissance
  - epxnee character varying(3): mention du complément - EPX ou NEE si complément
  - dnomcp character varying(30): Nom complément
  - dprncp character varying(15): Prénoms associés au complément
  - *topcdi character varying(1): top transalp (indisponible)*
  - *oriard character varying(1): origine adresse (indisponible)*
  - *fixard character varying(1): pérennité adresse (indisponible)*
  - *datadr character varying(8): date adresse (indisponible)*
  - *topdec character varying(1): origine décès (indisponible)*
  - *datdec character varying(4): date de décès (indisponible)*
  - dsiren character varying(10): numéro siren
  
* Indisponible

  - *ccmm character varying(1): création compte cadastral (indisponible)*
  - *topja character varying(1): indic jeune agriculteur (indisponible)*
  - *datja date: date jeune agriculteur (indisponible)*
  - *anospi character varying(3): ano transalp (indisponible)*
  - *cblpmo character varying(1): code blocage caractère personne morale (indisponible)*
  - *gtodge character varying(1): top appartenance à la DGE (indisponible)*
  - *gpctf character varying(1): top paiement centralisé à la taxe foncière (indisponible)*
  - *gpctsb character varying(1): top paiement centralisé à la TSBCS (indisponible)*
  - *jmodge character varying(2): mois d’entrée à la DGE (indisponible)*
  - *jandge character varying(4): année d’entrée à la DGE (indisponible)*
  - *jantfc character varying(4): année d’entrée paiement TF (indisponible)*
  - *jantbc character varying(4): année d’entrée paiement TSBCS (indisponible)*
  
* Autre

  - lot character(3): lot d'importation des données

Champs clés étrangères :

- :ref:`comptecommunal` : comptecommunal character varying(12) -  référence au compte communal auquel le propriétaire appartient
- ccodro_ character varying(1): code du droit réel ou particulier - Nouveau code en 2009 : C (fiduciaire)
- ccodem_ character varying(1): code du démembrement/indivision - C S L I V
- gtoper_ character varying(1): indicateur de personne physique ou morale - 1 = physique, 2 = morale
- ccoqua_ character varying(1): Code qualité de personne physique - 1, 2 ou 3
- dnatpr_ character varying(3): Code nature de personne physique ou morale
- ccogrm_ character varying(2): Code groupe de personne morale - 0 à 9 - 0A à 9A
- dforme_ character varying(7): forme juridique abrégée majic2 X Données Générales
- gtyp3_ character varying(1): type de la 3eme ligne d’adresse
- gtyp4_ character varying(1): Type de la 4eme ligne d’adresse
- gtyp5_ character varying(1): type de la 5eme ligne d’adresse
- gtyp6_ character varying(1): type de la 6eme ligne d’adresse

Lien(s) cartographique(s) :

- néant

Table(s) dépendantes(s) :

- néant

------------------
Les nomenclatures
------------------
.. _ccodro:

- ccodro: valeur du droit réel ou particulier exercé par le libellé partiel

.. _ccodem:

- ccodem: nature du démembrement pour les libellés partiels de même code droit

.. _dforme:

- dforme : forme juridique des personnes morales (nomenclature INSEE +F001 (les copropriétaires) et F002 (les associés d’une SCI).

.. _gtoper:

- gtoper: indicateur de personne physique ou morale: 1 = physique, 2 = morale

.. _ccoqua: 

- ccoqua: code qualité de personne physique

.. _dnatpr:

- dnatpr: Nature de la personne pour personnes physique et morale

.. _ccogrm:

- ccogrm: Groupe de personne morale classés en 10 groupes numérotés de 0 à 9

.. _gtyp3:

- gtyp3: indicateur de contenu ligne d'adresse 3: ligne d’adresse ou Complément d'adresse, typeVilla, Résidence, Chez M. X,etc)

.. _gtyp4:

- gtyp4: indicateur de contenu ligne d'adresse 4: ligne d'adresse indiquant la voie et la voirie.

.. _gtyp5:

- gtyp5: indicateur de contenu ligne d'adresse 5: ligne complément concernant la commune

.. _gtyp6:

- gtyp6: indicateur de contenu ligne d'adresse 6: ligne du code postal
