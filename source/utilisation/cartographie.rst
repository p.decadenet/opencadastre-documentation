.. _cartographie:

############
Cartographie
############

.. _carto_integre:

********************************************************
Plan intégré dans l'interface applicative d'OpenCadastre
********************************************************

OpenCadastre propose l'affichage interactive du plan cadastral numérisé.

Chaque enregistrement lié à une donnée géographique donne accès à sa visualisation en cliquant sur lu bouton correspondant


.. image:: ../_static/utilisation/util_ergo_nav_sig.jpg

.. image:: ../_static/utilisation/geo_parcelle_sig.jpg

Vous pouvez également à partir de la carte accèder à l'ensemble des informations situé sur à point donné par un simple clic sur la carte:

.. image:: ../_static/utilisation/sig_getfeaturesinfo.jpg

Nous avons, par exemple la possibilité d'afficher le formulaire de la parcelle correspondante en cliquant sur "fiche complète" :

.. image:: ../_static/utilisation/parcelleBS0158.jpg

Ou faire afficher la fiche parcelle synthétique en cliquant sur "fiche synthétique"

.. image:: ../_static/utilisation/parcelle_synthetique.jpg

.. _carto_flux:

********
Flux WMS 
********

OpenCadastre met à disposition un flux WMS intégrant plusieurs couches (en gras) proposant, pour la plupart, le service GetFeatureInfo (GFI).

l'adresse du flux est le même que celui indiqué dans l'installation de l'application :ref:`sgbd`.

Les couches sont les suivantes:

**unite_fonciere**
  **geo_label_parcelle_uf** (GFI)  
    - :ref:`geo_label` - geom(Point)
    - restriction: 'PARCELLE_id' AND "geo_parcelle" IN (select geo_parcelle from opencadastre.geo_parcelle where geom_uf is not null) and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **geo_parcelle_uf** (GFI)
    - :ref:`geo_parcelle` - geom_uf(MultiPolygon)
    - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')
	
**Cadastre**
  **detail_topographique**
    **geo_label_voiep** (GFI)
      - :ref:`geo_label` - geom(Point)
      - restriction: ogr_obj__1 = 'VOIEP_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **numvoie**
      **geo_label_num_voie** (GFI)
        - :ref:`geo_label` - geom(Point)
        - restriction: ogr_obj__1='NUMVOIE_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **tline**
      **geo_tline** (GFI)
        - :ref:`geo_tline` - geom(MultiLineString)
        - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

      **geo_label_tline** (GFI)
        - :ref:`geo_label` - geom(Point)
        - restriction: ogr_obj__1 = 'TLINE_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **tpoint**
      **geo_label_tpoint** (GFI)
        - :ref:`geo_label` - geom(Point)
        - restriction: ogr_obj__1 = 'TPOINT_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

      **geo_tpoint** (GFI)
        - :ref:`geo_tpoint` - geom(Point)
        - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **tsurf**
      **geo_label_tsurf** (GFI)
        - :ref:`geo_label` - geom(Point)
        - restriction: ogr_obj__1 = 'TSURF_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

      **geo_tsurf_a34** (GFI)
        - :ref:`geo_tsurf` - geom(MultiPolygon)
        - restriction: geo_sym = '34' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

      **geo_tsurf_s34** (GFI)
        - :ref:`geo_tsurf` - geom(MultiPolygon)
        - restriction: geo_sym != '34' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **tronfluv**
      **geo_label_tronfluv** (GFI)
        - :ref:`geo_label` - geom(Point)
        - restriction: ogr_obj__1 = 'TRONFLUV_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

      **geo_tronfluv** (GFI)
        - :ref:`geo_tronfluv` - geom(MultiPolygon)
        - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **symbole_limite_propriete**
    **geo_symblim** (GFI)
      - :ref:`geo_symblim` - geom(Point)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **point_canevas**
    **geo_ptcanv** (GFI)
      - :ref:`geo_ptcanv` - geom(Point)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **croix**
    **geo_croix** (GFI)
      - :ref:`geo_croix` - geom(Point)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **borne**
    **geo_borne** (GFI)
      - :ref:`geo_borne` - geom(Point)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **section**
    **geo_label_section** (GFI)
      - :ref:`geo_label` - geom(Point)
      - restriction: ogr_obj__1 = 'SECTION_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **geo_section** (GFI)
      - :ref:`geo_section` - geom(MultiPolygon)

  **lieudit**
    **geo_label_lieudit** (GFI)
      - :ref:`geo_label` - geom(Point)
      - restriction: ogr_obj__1 = 'LIEUDIT_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **geo_lieudit** (GFI)
      - :ref:`geo_lieudit` - geom(MultiPolygon)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **parcelle**
    **geo_label_parcelle** (GFI)
      - :ref:`geo_label` - geom(Point)
      - restriction: ogr_obj__1 = 'PARCELLE_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **geo_parcelle** (GFI)
      - :ref:`geo_parcelle` - geom(MultiPolygon)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')
  **batiment**
    **geo_batiment** (GFI)
      - :ref:`geo_batiment` - geom(MultiPolygon)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')
  **subdivision_fiscale**
    **geo_label_subdfisc** (GFI)
      - :ref:`geo_label` - geom(Point)
      - restriction: ogr_obj__1 = 'SUBDFISC_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **geo_subdfisc** (GFI)
      - :ref:`geo_subdfisc` - geom(MultiPolygon)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')
  **subdivision_section**
    **geo_subdsect** (GFI)
      - :ref:`geo_subdsect` - geom(MultiPolygon)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **zoncommuni**
    **geo_label_zoncommuni** (GFI)
      - :ref:`geo_label` - geom(Point)
      - restriction: ogr_obj__1 = 'ZONCOMMUNI_id' and annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

    **geo_zoncommuni** (GFI)
      - :ref:`geo_zoncommuni` - geom(MultiLinestring)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

  **commune**
    **geo_commune** (GFI)
      - :ref:`geo_commune` - geom(MultiPolygon)
      - restriction: annee in (select valeur from opencadastre.om_parametre where om_parametre='3')

** export **
  **dgi_adresse_uf**
    - :ref:`dgi_adresse_uf` - geom(MultiPolygon)

  **dgi_adresse_parcelle**
    - :ref:`dgi_adresse_parcelle` - geom(MultiPolygon)

  **dgi_batiment**
    - :ref:`dgi_batiment` - geom(MultiPolygon)

  **dgi_uf**
    - :ref:`dgi_uf` - geom(MultiPolygon)

  **dgi_parcelle**
    - :ref:`dgi_parcelle` - geom(MultiPolygon)





