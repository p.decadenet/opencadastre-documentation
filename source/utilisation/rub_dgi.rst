.. _rub_dgi:

#########################
Table d'exportation dgi_*
#########################

Les tables dgi_* sont vidées et remplies par le traitement :ref:`choixexercice` qui duplique plusieurs tables en les filtrant sur le seul exercice choisi lors du lancement du traitement.

Elles sont destinées à être utilisées dans d'autres applications comme openMairie.

Par exemple, OpenAdresse utilise les tables dgi_adresse_parcelle et dgi_adresse_uf pour associer une emprise à un point adresse en s'appuyant sur les codes rivoli.

Plus généralement, dans les applications openMairie utilisant les fonctionnalités sig intégrées, elles servent à alimenter les paniers, fournissant ainsi des géométries toutes prêtes, qui peuvent être assemblées pour représenter des objets métiers.

.. _dgi_parcelle:

************
dgi_parcelle
************

- **dgi_parcelle** bigint NOT NULL: incrément identifiant la parcelle
- code text: [ccosec][dnupla] sur 6 caractères
- code_insee text: [ccodep][code_insee de la commune] sur 5 caractères
- section character varying(2): [ccosec]
- dnupla character varying(4): [dnupla]
- geom geometry(MultiPolygon,2154): géométrie de la parcelle

Requète d'alimentation de la table: ::

    INSERT INTO dgi_parcelle 
    SELECT 
      row_number() OVER(ORDER BY geo_parcelle DESC) AS dgi_parcelle, 
      code,
      code_insee,
      section,
      dnupla,
      geom
    FROM (
        SELECT 
          g.geo_parcelle AS geo_parcelle, 
          p.ccosec      p.dnupla AS code, 
          p.ccodep      p.ccocom AS code_insee, 
          p.ccosec AS section, 
          p.dnupla AS dnupla, 
          g.geom AS geom
        FROM geo_parcelle g
        JOIN parcelle p ON  g.parcelle = p.parcelle
        JOIN om_parametre o ON p.annee = o.valeur AND o.libelle = 'année'
      UNION
        SELECT g.geo_parcelle AS geo_parcelle,
          replace(s.tex,'0',' ')    substring(g.idu,9,4) AS code,
          substring(c.commune,5,2)    substring(c.commune,8,3) AS code_insee,
          replace(s.tex,'0',' ') AS section,
          substring(g.idu,9,4) AS dnupla,
          g.geom AS geom
        FROM geo_parcelle g
        JOIN geo_section s ON g.geo_section=s.geo_section 
        JOIN geo_commune c ON s.geo_commune=c.geo_commune
        JOIN om_parametre o ON g.annee = o.valeur  AND o.libelle = 'année'
        WHERE g.parcelle IS NULL OR g.parcelle=''
    ) a

.. _dgi_batiment:

************
dgi_batiment
************

- **dgi_batiment** bigint NOT NULL: incrément identifiant l'unité foncière
- code character varying(14): [geo_batiment] clé primaire dans geo_batiment (cette valeur change chaque année)
- type text: dévivé de geo_dur (:ref:`geo_batiment`)


- geom geometry(MultiPolygon,2154): géométrie du bâtiment

Requète d'alimentation de la table: ::

  INSERT INTO dgi_batiment
  SELECT
    row_number() OVER(ORDER BY b.geo_batiment DESC) AS dgi_batiment,
    b.geo_batiment AS code,
    CASE WHEN geo_dur='01' THEN 'bâti dur' ELSE 'bâti léger' END AS type, 
    b.geom AS geom
  FROM geo_batiment b
  JOIN om_parametre o ON b.annee = o.valeur AND o.om_parametre = 3

.. _dgi_uf:

******
dgi_uf
******

- **dgi_uf** bigint NOT NULL: incrément identifiant l'unité foncière
- code text: [ccosec][dnupla] sur 6 caractères
- code_insee text: [ccodep][code_insee de la commune] sur 5 caractères
- section character varying(2): [ccosec]
- dnupla character varying(4): [dnupla]
- geom geometry(MultiPolygon,2154): géométrie de la parcelle

Requète d'alimentation de la table: ::

    INSERT INTO dgi_uf
    SELECT
      row_number() OVER(ORDER BY g.geo_parcelle DESC) AS dgi_uf, 
      p.ccosec || p.dnupla AS code, 
      p.ccodep || p.ccocom AS code_insee,
      p.ccosec as section, 
      p.dnupla as dnupla,
      g.geom_uf AS geom
    FROM geo_parcelle g 
	JOIN parcelle p ON g.parcelle = p.parcelle 
    JOIN om_parametre o ON p.annee = o.valeur AND o.om_parametre = 3
    WHERE geom_uf is not null

.. _dgi_adresse_parcelle:

********************
dgi_adresse_parcelle
********************

- **dgi_adresse_parcelle** bigint NOT NULL: incrément identifiant la ligne
- parcelle text: code parcelle [ccosec][dnupla] sur 6 caractères
- code_insee text: [ccodep][code_insee de la commune] sur 5 caractères
- section character varying(2): [ccosec] de la parcelle
- dnupla character varying(4):[dnupla] de la parcelle
- code_rivoli text: code rivoli de la voie [ccodep][code_insee de la commune][ccoriv]
- numero numeric: [dnvoiri] numéro dans la rue
- repetition text: [dindic] indice de répétition (BIS, TER, ...)
- lib_voie text: [dvoilib] libellé de la voie
- adresse text: [numero] [repetition] [lib_voie]
- affichage text: [numero] [repetition]
- cle_rivoli text: [-][code_rivoli][-]
- cle_rivoli_numero text: [-][code_rivoli][-][numero][-]
- cle_rivoli_numero_rep text: [-][code_rivoli][-][numero][-][repetition][-]
- multi_adresse integer: nombre d'adresses adossées à la parcelle
- geom geometry(MultiPolygon:2154): géométrie de l'emprise de l'adresse

Requète d'alimentation de la table: ::

  INSERT INTO dgi_adresse_parcelle
  SELECT
    row_number() OVER(ORDER BY parcelle DESC) AS dgi_adresse_parcelle,
    parcelle, 
    code_insee,
    section,
    dnupla,
    code_rivoli,
    numero,
    repetition,
    lib_voie,
    numero||' '||
      CASE WHEN repetition='' 
      THEN '' 
      ELSE  repetition||' ' END||
      lib_voie AS adresse, 
    numero||' '||
      CASE WHEN repetition='' 
      THEN '' 
      ELSE  repetition END AS affichage,
    '-'||code_rivoli||'-' AS cle_rivoli,
    '-'||code_rivoli||'-'||numero||'-' AS cle_rivoli_numero,
    '-'||code_rivoli||'-'||numero||'-'||repetition||'-' AS cle_rivoli_numero_rep,
    1 as multi_adresse,
    geom
  FROM (
    SELECT DISTINCT
      l.ccosec || l.dnupla AS parcelle,
      l.ccodep || l.ccocom AS code_insee,
      l.ccosec AS section,
      l.dnupla AS dnupla,
      l.ccodep || l.ccocom||l.ccoriv AS code_rivoli,
      to_number(
        CASE WHEN trim(dnvoiri)='' 
        THEN '0'
      ELSE trim(dnvoiri) END,'0000') AS numero,
      CASE
        WHEN trim(l.dindic) IN ('','-','6','7') THEN ''
        WHEN trim(l.dindic)='3' THEN 'TER'
        WHEN trim(l.dindic)='4' THEN 'QUATER'
        WHEN trim(l.dindic)='5' "THEN 'CINQUIES'
        ELSE trim(l.dindic)
      END AS repetition,
      trim(l.dvoilib) AS lib_voie,
      g.geom AS geom
    FROM local00 l
    JOIN om_parametre p ON om_parametre=3 AND l.annee=p.valeur 
    JOIN DB_PREFIXE."geo_parcelle g ON g.parcelle=l.parcelle
  ) AS v

.. _dgi_adresse_uf:

**************
dgi_adresse_uf
**************

- **dgi_adresse_uf** bigint NOT NULL: : incrément identifiant la ligne
- parcelle text: code parcelle de l'unité foncière [ccosec][dnupla] sur 6 caractères
- code_insee text: [ccodep][code_insee de la commune] sur 5 caractères
- section character varying(2): [ccosec] de l'unité foncière
- dnupla character varying(4):[dnupla] de l'unité foncière
- code_rivoli text: code rivoli de la voie [ccodep][code_insee de la commune][ccoriv]
- numero numeric: [dnvoiri] numéro dans la rue
- repetition text: [dindic] indice de répétition (BIS, TER, ...)
- lib_voie text: [dvoilib] libellé de la voie
- adresse text: [numero] [repetition] [lib_voie]
- affichage text: [numero] [repetition]
- cle_rivoli text: [-][code_rivoli][-]
- cle_rivoli_numero text: [-][code_rivoli][-][numero][-]
- cle_rivoli_numero_rep text: [-][code_rivoli][-][numero][-][repetition][-]
- multi_adresse integer: nombre d'adresses adossées à l'unité foncière
- geom geometry(MultiPolygon:2154): géométrie de l'emprise de l'adresse

Requète d'alimentation de la table: ::

  INSERT INTO dgi_adresse_uf
  SELECT
    row_number() OVER(ORDER BY parcelle DESC) AS dgi_adresse_uf,
    parcelle,
    code_insee,
    section,
    dnupla,
    code_rivoli,
    numero,
    repetition,
    lib_voie,
    adresse,
    affichage,
    cle_rivoli,
    cle_rivoli_numero,
    cle_rivoli_numero_rep,
    1 as multi_adresse,
    geom
  FROM (
    SELECT DISTINCT
      parcelle,
      code_insee,
      section,
      dnupla,
      code_rivoli,
      numero,
      repetition,
      lib_voie,
      numero||' '||
        CASE WHEN repetition='' THEN '' ELSE  repetition||' ' END||
        lib_voie AS adresse,
      numero||' '||
        CASE WHEN repetition='' THEN '' ELSE  repetition END 
        AS affichage,
      '-'||code_rivoli||'-' AS cle_rivoli,
      '-'||code_rivoli||'-'||numero||'-' AS cle_rivoli_numero,
      '-'||code_rivoli||'-'||numero||'-'||repetition||'-' 
        AS cle_rivoli_numero_rep,
      geom
    FROM (
        SELECT DISTINCT
          l.ccosec || l.dnupla AS parcelle,
          l.ccodep || l.ccocom AS code_insee,
          l.ccosec AS section,
          l.dnupla AS dnupla,
          l.ccodep || l.ccocom||l.ccoriv AS code_rivoli,
          to_number(
            CASE WHEN trim(dnvoiri)=''
            THEN '0' ELSE trim(dnvoiri) END,
            '0000') AS numero,
          CASE
            WHEN trim(l.dindic) IN ('','-','6','7') THEN ''
            WHEN trim(l.dindic)='3' THEN 'TER'
            WHEN trim(l.dindic)='4' THEN 'QUATER'
            WHEN trim(l.dindic)='5' THEN 'CINQUIES'
            ELSE  trim(l.dindic)
          END AS repetition,
          trim(l.dvoilib) AS lib_voie,
          g.geom_uf AS geom
        FROM local00 l
        JOIN om_parametre p ON om_parametre=3 AND l.annee=p.valeur 
        JOIN geo_parcelle g ON g.parcelle=l.parcelle
        WHERE l.parcelle NOT IN (SELECT parcellea FROM parcellecomposante)
      UNION
        SELECT DISTINCT
          uf.ccosec || uf.dnupla AS parcelle,
          uf.ccodep || uf.ccocom AS code_insee,
          uf.ccosec AS section,
          uf.dnupla AS dnupla,
          l.ccodep || l.ccocom||l.ccoriv AS code_rivoli,
          to_number(
            CASE WHEN trim(l.dnvoiri)='' 
            THEN '0' ELSE trim(l.dnvoiri) END,
            '0000') AS numero
          CASE
            WHEN trim(l.dindic) IN ('','-','6','7') THEN ''
            WHEN trim(l.dindic)='3' THEN 'TER'
            WHEN trim(l.dindic)='4' THEN 'QUATER'
            WHEN trim(l.dindic)='5' THEN 'CINQUIES'
            ELSE  trim(l.dindic)
          END AS repetition,
          trim(l.dvoilib) AS lib_voie,
          g.geom_uf AS geom
        FROM local00 l
        JOIN parcellecomposante pc ON pc.parcellea=l.parcelle
        JOIN om_parametre p ON om_parametre=3 AND l.annee=p.valeur 
        JOIN parcelle uf ON pc.parcelle=uf.parcelle
        JOIN geo_parcelle g ON g.parcelle=uf.parcelle
      ) 
    b)
  c
