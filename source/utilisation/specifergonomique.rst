.. _specifergonomique:

#########################
Spécificités ergonomiques
#########################

Cette application propose l'intégration des données majic 3 et du plan cadastral numérisé au format Edigeo dans un modèle **relationnels**.
Ces données n'ayant pas vocation a être modifiées, les actions de modification dans les formulaires ont été supprimées.

.. image:: ../_static/utilisation/util_ergo_form.jpg

Le modèle relationnel d'openCadastre étant complexe (près de 100 tables) l'interface propose une navigation simplifiée entre les données liées.

Par exemple pour ouvrir le formulaire de la pev correspondant à la place du formulaire courant, cliquer sur le bouton situé devant la référence

.. image:: ../_static/utilisation/util_ergo_nav_dans.jpg

ou, pour l'ouvrir dans un nouvel onglet, sur le bouton 

.. image:: ../_static/utilisation/util_ergo_nav_nouv.jpg

Une clé étrangère peut faire référence à un enregistrement contenant des données géographiques, pour ouvrir la carte correspondante cliquer sur le bouton 

.. image:: ../_static/utilisation/util_ergo_nav_sig.jpg
 
dans l'exemple, une commune majic3 est liée à sa représentation dans le plan cadastral numérisé geo_commune

.. image:: ../_static/utilisation/util_ergo_sig.jpg
